<?php

// Kleines Script zum Anzeigen der json-Dateien des Ultimate Mussion Makers
// zeigt nicht alles an, was in den Dateien steht, soll nur eine übersicht verschaffen, welche Missionen mit welchen Einstellungen enthalten sind.
//
// Simple script to view the json-Files of the Ultimate Mission Maker
// ohnely a simple overview over the missions will be output
//
// Jan Clauß 2022-04-09
//
// https://gitlab.com/jan.clauss/UMMview

?>

<!doctype html>
<html lang='de'>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>janc70 - UMMview</title>
	</head>
	<body>
		Sende eine Datei oder füge den Quelltext in das Textfeld ein.<br>
		Send a file or paste the source text into the text field.<br><br>
		<form enctype="multipart/form-data" method='post'>
			<input type='file' name='ummjson'><br>
			<textarea name='jsontext'></textarea><br>
			<input type='submit' name='send' value='Auswerten'>
		</form>
<?php
if(IsSet($_POST['jsontext']) and trim($_POST['jsontext']) <> '') {
	$json=trim($_POST['jsontext']);
}elseif(IsSet($_FILES['ummjson'])) { // Als Datei gesendet
	$json=file_get_contents($_FILES['ummjson']['tmp_name']);
}
if(!IsSet($json)) exit; // Nichts übergeben
if(($jsonarray=json_decode($json, true)) === null) {
	echo "	</body>\n";
	echo "	</html>\n";
	exit; // Kein JSON
}
echo "<hr>\n";
echo "<table>\n";
echo "<tr><td colspan=3>Mosaic Name:</td><td colspan=5>".$jsonarray['missionSetName']."</td></tr>\n";
echo "<tr><td colspan=3>Mosaic Description:</td><td colspan=5>".$jsonarray['missionSetDescription']."</td></tr>\n";
echo "<tr><td colspan=3>Planed Missions:</td><td colspan=5>".$jsonarray['plannedBannerLength']."</td></tr>\n";
echo "<tr><td colspan=3>Missions:</td><td colspan=5>".count($jsonarray['missions'])."</td></tr>\n";
$portallist=array();
$lastportal='';
foreach($jsonarray['missions'] AS $mission) {
	echo "<tr><td></td><td colspan=2>Mission</td><td colspan=4>".$mission['missionTitle']."</td></tr>\n";
	if(count($mission['portals']) < 6) {
		// Warnung, zu Wenig Portale
		echo "<tr><td colspan=2></td><td colspan=6 style='color:red;'>Only ".count($mission['portals'])." Portals</td></tr>\n";
	}
	$portalnumber=1;
	foreach($mission['portals'] AS $portal) {
		echo "<tr><td>&nbsp</td><td>&nbsp</td><td>Portal $portalnumber</td><td>".$portal['title']."</td><td>".$portal['objective']['type']."</td>";
		if($portal['objective']['type'] == 'PASSPHRASE') {
			echo "<td>".$portal['objective']['passphrase_params']['question']."</td><td>".$portal['objective']['passphrase_params']['_single_passphrase']."</td>";
		}
		echo "<td>";
		if($portal['objective']['type'] == 'HACK_PORTAL') {
			if($portal['guid'] == $lastportal) {
				echo "<span style='color:red;' title='Last Portal is the sam as first from next mission'>‼</span>";
			}
			$lastportal=$portal['guid'];
			if(!IsSet($portallist[$portal['guid']])) {
				$portallist[$portal['guid']]=1;
			}else{
				$portallist[$portal['guid']]++;
				echo $portallist[$portal['guid']];
			}
		}
		echo "</td>";
		echo "</tr>";
		$portalnumber++;
	}
}
echo "</table>";
echo "	</body>\n";
echo "	</html>\n";

?>